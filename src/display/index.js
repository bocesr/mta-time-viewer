const prompt = require('select-prompt');

const display = function(title, values) {
    return new Promise((resolve, reject) => {
        prompt(title, values)
        .on('submit', (value) => {
            resolve(value);
        })
        .on('abort', () => {
            resolve(null);
        });
    });
};

module.exports = display;