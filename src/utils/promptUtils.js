const stopUtils = require('./csv/stopUtils');
const directionUtils = require('./api/directionUtils');
const timeUtils = require('./api/timeUtils');

let promptUtils = {};

promptUtils.formatLines = function(lines) {
    return lines.map((id) => {
        return {
            title: id,
            value: id
        };
    });
};

promptUtils.formatStations = function(stations) {
    return stations.map((station) => {
        const id = stopUtils.getId(station);
        const name = stopUtils.getName(station);
        const location = stopUtils.getLocation(station);

        return {
            title: `${name} (${id}): ${location.lat}, ${location.lng}`,
            value: id
        };
    });
};

promptUtils.formatDirectionsTimes = function(directions) {
    return directions.flatMap((direction) => {
        const name = directionUtils.getName(direction)
        const times = directionUtils.getTimes(direction);

        return (times.length > 0) ? [`${name}:`, times.map((time) => {
            const route = timeUtils.getRoute(time);
            const minutes = timeUtils.getMinutes(time);
            return `\t${route}: ${minutes}`;
        }).join('\n')] : [];   
    }).join('\n');
};

module.exports = promptUtils;