const stopUtils = require('./stopUtils');
const LocationType = require("../../values/LocationType");

let stopsUtils = {};

stopsUtils.mapStations = function(stops) {
    let map = {};

    stops.forEach((stop) => {
        if (stopUtils.getType(stop) === LocationType.STATION) {
            const id = stopUtils.getId(stop);
            map[id] = stop;
        }
    });
    return map;
};

module.exports = stopsUtils;