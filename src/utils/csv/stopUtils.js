let stopUtils = {};

stopUtils.getId = function(stop) {
    return stop.stop_id;
};

stopUtils.getName = function(stop) {
    return stop.stop_name;
};

stopUtils.getType = function(stop) {
    return parseInt(stop.location_type);
}

stopUtils.getLocation = function(stop) {
    return {
        lat: stop.stop_lat,
        lng: stop.stop_lon
    };
}

module.exports = stopUtils;