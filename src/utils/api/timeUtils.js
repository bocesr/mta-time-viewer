let timeUtils = {};

timeUtils.getRoute = function(time) {
    return time.route;
};

timeUtils.getMinutes = function(time) {
    return time.minutes;
};

module.exports = timeUtils;