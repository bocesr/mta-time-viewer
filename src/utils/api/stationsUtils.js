let stationsUtils = {};

stationsUtils.getStations = function(stations) {
    // WARN: API RETURNS STRING FORMAT, NEEDS TO BE PARSED AGAIN
    if (typeof stations === "string") {
        stations = JSON.parse(stations);
    }

    return stations.reduce((acc, borough) => {
        return acc.concat(borough.stations);
    }, []);
};

module.exports = stationsUtils;