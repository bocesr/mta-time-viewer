let stationUtils = {};

stationUtils.getName = function(station) {
    return station.name;
};

stationUtils.getId = function(station) {
    return station.id;
};

module.exports = stationUtils;