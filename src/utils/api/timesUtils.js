const timeUtils = require('./timeUtils');

let timesUtils = {};

timesUtils.getMinutes = function(times) {
    return times.map((time) => {
        return timeUtils.getMinutes(time);
    });
};

module.exports = timesUtils;