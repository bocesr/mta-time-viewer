const directionUtils = require('./directionUtils');

let directionsUtils = {};

directionsUtils.getDirection1 = function(directions) {
    return directions.direction1;
}

directionsUtils.getDirection2 = function(directions) {
    return directions.direction2;
};

directionsUtils.getDirections = function(directions) {
    const direction1 = directionsUtils.getDirection1(directions);
    const direction2 = directionsUtils.getDirection2(directions);

    return [direction1, direction2];
};

module.exports = directionsUtils;