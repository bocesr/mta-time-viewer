// LINES UTILS

const lineUtils = require("./lineUtils");

let linesUtils = {};

linesUtils.getIds = function(lines) {
    return lines.map((line) => {
        return lineUtils.getId(line);
    });
};

module.exports = linesUtils;