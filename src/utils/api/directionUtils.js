const timeUtils = require('./timeUtils');

let directionUtils = {};

directionUtils.getName = function(direction) {
    return direction.name;
}

directionUtils.getTimes = function(direction) {
    return direction.times;
};

module.exports = directionUtils;