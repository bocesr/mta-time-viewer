const config = require('../../config');
const routes = require('./routes');
const request = require('./request');

let api = {};

api.getLines = function() {
    const url = [config.baseurl, routes.getLines].join("/");

    return request(url);
};

api.getStations = function(line) {
    const url = [config.baseurl, routes.getStations, line].join("/");

    return request(url);
};

api.getTime = function(line, station) {
    const url = [config.baseurl, routes.getTime, line, station].join("/");

    return request(url);
};

module.exports = api;