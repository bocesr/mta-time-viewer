const http = require('http');

const request = function(url) {
    return new Promise((resolve, reject) => {
        let data = "";

        http.get(url, (res) =>{
            res.on('data', (chunk) => {
                data += chunk;
            });

            res.on('end', () => {
                try {
                    let json = JSON.parse(data);
                    resolve(json);
                } catch (err) {
                    reject(err);
                }
            });
        }).on('error', (err) => {
            reject(err);
        });
    });
};

module.exports = request;