// API ROUTES

const routes = {
    getLines: "getSubwaylines",
    getStations: "getStationsByLine",
    getTime: "getTime"
};

module.exports = routes;