const api = require("./api");
const csv = require('./csv');
const linesUtils = require("./utils/api/linesUtils");
const stopsUtils = require('./utils/csv/stopsUtils');
const apiStationsUtils = require('./utils/api/stationsUtils');
const apiStationUtils = require('./utils/api/stationUtils');
const directionsUtils = require('./utils/api/directionsUtils');

class MTANetwork {

    // Constructor
    constructor() {
        this.lines = null;
        this.stations = null;
    }

    // Methods
    async loadResources() {
        await this._loadLines();
        await this._loadCSV();
    }

    async _loadLines() {
        const lines = await api.getLines();
        
        this.lines = linesUtils.getIds(lines);
    }

    async _loadCSV() {
        const stops = await csv.getStops();
        const stations = stopsUtils.mapStations(stops);

        this.stations = stations;
    }

    getLines() {
        return this.lines;
    }

    async getStations(line) {
        const apiStations = await api.getStations(line);
        const stations = apiStationsUtils.getStations(apiStations);

        return stations.flatMap((station) => {
            const id = apiStationUtils.getId(station);
            const stop = this.stations[id];

            return stop !== undefined ? [stop] : [];
        });
    }

    async getTimes(line, station) {
        const times = await api.getTime(line, station);
        const directions = directionsUtils.getDirections(times);

        return directions;
    }

}

module.exports = MTANetwork;