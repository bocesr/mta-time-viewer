const config = require('../../config');
const csvtojson = require('csvtojson');

let reader = {};

reader.getStops = function() {
    const path = [config.csvpath, "stops.txt"].join("/");

    return csvtojson().fromFile(path);
};

module.exports = reader;