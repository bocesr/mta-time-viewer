
const LocationType = {
    STOP: 0,
    STATION: 1,
    DOOR: 2
};

module.exports = LocationType;