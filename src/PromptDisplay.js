const display = require('./display');
const promptUtils = require('./utils/promptUtils');

class PromptDisplay {

    // Methods
    promptLines(lines) {
        const title = "Please choose a line among the following ones:";
        const values = promptUtils.formatLines(lines);
        return display(title, values);
    }

    promptStations(stations) {
        const title = "Please choose a station to consult time:";
        const values = promptUtils.formatStations(stations);
        return display(title, values);
    }

    promptDirectionsTimes(directions) {
        const times = promptUtils.formatDirectionsTimes(directions);
        
        console.log(times);
    }

    async shouldContinue() {
        const title = "Would you like to consult more stations?";
        const values = [
            { title: "Show lines", value: true },
            { title: "Exit", value: false }
        ];
        return (await display(title, values)) || false;
    }
}

module.exports = PromptDisplay;