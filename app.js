const MTANetwork = require("./src/MtaNetwork");
const PromptDisplay = require('./src/PromptDisplay');

const mtaNetwork = new MTANetwork();
const promptDisplay = new PromptDisplay();

async function run() {
    const lines = mtaNetwork.getLines();
    const line = await promptDisplay.promptLines(lines);
    if (line === null) {
        return false;
    }

    const stations = await mtaNetwork.getStations(line);
    const station = await promptDisplay.promptStations(stations); 
    if (station === null) {
        return true;
    }

    const times = await mtaNetwork.getTimes(line, station);
    promptDisplay.promptDirectionsTimes(times);

    return (await promptDisplay.shouldContinue());
}

(async function() {
    try {
        await mtaNetwork.loadResources();
        while (await run());
    } catch (e) {
        console.error("An error occured:", e);
    }
})();